const db = require('./bot/db');
const ready = require('./bot/ready');
const members = require('./bot/member');
const msg = require('./bot/message');
const timers = require('./bot/timers');
const Discord = require('discord.js');
const bot = new Discord.Client({ autoReconnect: true });

let pool = db.gbot_pool();
let site_pool = db.site_pool();

let prefix = "!";

bot.on('ready', () => {
    ready.setUp(bot);
    TestChannel.send(`I'm online again. <@227607583106662400>`);
});

bot.on('guildMemberAdd', member => {
    members.welcome(bot, member, pool, RolesAddable, Emojis);
});

bot.on('guildMemberRemove', member => {
    members.deleteMember(bot, member, pool);
});

bot.on('guildMemberUpdate', (oldMember, newMember) => {
    members.newRole(bot, oldMember, newMember);
})

timers.setAll(bot, pool);

bot.on('message', message => {
    if(message.author.bot) { return; }
    msg.route(bot, message, pool, site_pool, RolesAddable, prefix);
});

bot.login(db.login());