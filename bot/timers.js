const ready = require('./ready');
const msg = require('./message');
const later = require('later');
const reminder = require('./msgs/staff/reminder');

function bday(pool) { // internal
	let tester = later.parse.cron('0 5 * * ? *');
	let test_timer = later.setInterval(function() {
		msg.checkBday(pool);
	}, tester);
}

function dbReminders() {
	//
}

// function cleaner(pool) {
// 	let cleaner = later.parse.cron('0 5 * * 1 *'); // every Sunday morning at midnight
// 	let cleaner_timer = later.setInterval(function() {
// 		reminder.cleanDatabase(pool);
// 	}, cleaner);
// }

function trello(bot) { // internal
	let trello = later.parse.cron('0 1 * * 2 *'); // every Monday at 8pm
	let trello_timer = later.setInterval(function() {
		let GoG = bot.guilds.find(x => x.name === 'Guild of Geeks');
		let StaffChannel = GoG.channels.find(x => x.name === 'staff-general');
		StaffChannel.send(`This is your friendly weekly reminder to check `
			+ `Trello and see if there's anything on your to do list. :) \n`
			+ `<@&537782457018089483>`)
	}, trello);
}

function oilcan(bot) { // internal
	let oilcan = later.parse.cron('30 23 * * 1 *'); // every Monday at 6:30pm
	let oilcan_timer = later.setInterval(function() {
		let GoG = bot.guilds.find(x => x.name === 'Guild of Geeks');
		let MediaChannel = GoG.channels.find(x => x.name === 'social-media-feed');
		MediaChannel.send(`It's time to advertise for the Oilcan Harry's events `
			+ `tonight: podcast, trivia, tourney. <@&539590308124557322> `
			+ `<@&453430066039357442> <@227607583106662400>`)
	}, oilcan);
}

function ff(bot) { // internal
	let ff = later.parse.cron('30 23 * * 5 *'); // every Friday at 6:30pm
	let ff_timer = later.setInterval(function() {
		let GoG = bot.guilds.find(x => x.name === 'Guild of Geeks');
		let MediaChannel = GoG.channels.find(x => x.name === 'social-media-feed');
		MediaChannel.send(`Don't forget to put Friday Freeplay ads out! :) `
			+ `<@&539590308124557322> <@&453430066039357442>`)
	}, ff);
}

function anime(bot) { // internal
	let anime = later.parse.cron('30 23 * * 6 *'); // every Saturday at 6:30pm
	let anime_timer = later.setInterval(function() {
		let GoG = bot.guilds.find(x => x.name === 'Guild of Geeks');
		let MediaChannel = GoG.channels.find(x => x.name === 'social-media-feed');
		MediaChannel.send(`Get hype, let's ad for this anime! `
			+ `<@&539590308124557322> <@&453430066039357442>`)
	}, anime);
}

function setAll(bot, pool) { // external
	later.date.localTime();
	bday(pool);
	trello(bot);
	oilcan(bot);
	ff(bot);
	anime(bot);
}

module.exports = { setAll }