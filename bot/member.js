const ready = require('./ready');
const update = require('./update-member');

function newRole(bot, oldMember, newMember) {
    ready.setRoles(bot);
    update.newRole(bot, oldMember, newMember);
}

function welcomeRoleList(RolesAddable, Emojis) {
    let welcomeRoles = [];
    let temp_roles = [];
    for(i=0; i<RolesAddable.length; i++) {
        temp_roles.push(RolesAddable[i])
    }
    temp_roles.reverse().forEach(function(element) {
        line = ` - \`\`${element}\`\``
        if(Object.keys(Emojis).indexOf(element) != -1) {
            line += ` ${Emojis[`${element}`]}`
        }
        welcomeRoles.push(line)
    })
    return welcomeRoles.join('\n')
}

function welcome(bot, member, pool, RolesAddable, Emojis) {
    ready.setRoles(bot);

let welcome_msg = `Hello ${member.user}, welcome to the Guild of Geeks server! `
+ 'Please check out the <#521552516807131146> channel to see who we are \
and our server rules.\n\nIf you want to mute any text channels, you can do \
so by right-clicking the channel and selecting \"mute channel.\"\n\nPlease \
type \`\`!roles\`\` and the role you need: \n'
+ `${welcomeRoleList(RolesAddable, Emojis)}`
+ '\nso we can open up channels that are relevant to you.\nFor example: \
\`\`\`!roles Utena\`\`\`If you choose RWBY, Utena, MHA, Gen:LOCK, or any \
Spoilers role, please be prepared for the spoilers in those text channels.';

    WelcomeChannel.send(welcome_msg)

    pool.query(`INSERT INTO users (user_id, username)
        VALUES ("${member.user.id}", "${member.user.username}")`, function(err) {
            if(err) throw err;
        })
}

function deleteMember(bot, member, pool) {
    pool.query(`DELETE FROM users WHERE user_id = "${member.user.id}"`, function(err) {
        if(err) throw err;
    })
}

module.exports = { welcome, newRole, deleteMember }