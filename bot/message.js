let ready = require('./ready');
let staff = require('./msgs/staff/staff');
let roll = require('./msgs/roll');
let bday = require('./msgs/bday');
let roles = require('./msgs/roles');
let test = require('./msgs/testing');
let dt = require('./msgs/datetime');
let site = require('./msgs/staff/site');
let eid = require('./msgs/staff/eid');

function route(bot, message, pool, site_pool, RolesAddable, prefix) { // export
    if(message.channel.name === 'music-bot') {
        filterGroovy(message);
    }
    else if(message.content.startsWith(prefix)) {
        if(message.channel.type == 'dm') {
            DMrespond(bot, message, pool);
        }
        else {
            respond(bot, message, pool, site_pool, RolesAddable);
        }
    }
    else if(message.author.id == '227607583106662400' && message.content.startsWith('test')) {
        test.testMsg(bot, message, pool, RolesAddable);
    }
    else {
        trigger(bot, message);
    }
}

function respond(bot, message, pool, site_pool, RolesAddable) { // internal
    let input = message.content.toLowerCase().slice(1, message.content.length);
    switch (input.split(" ")[0]) {
        case "ping":
            message.channel.send("Pong!");
            break;
        case "help":
            helpList(message);
            break;
        case "list":
            helpList(message);
            break;
        case "role":
            ready.setRoles(bot);
            roles.updateRole(message, RolesAddable);
            break;
        case "roles":
            ready.setRoles(bot);
            roles.updateRole(message, RolesAddable);
            break;
        case "roll":
            roll.doRoll(message);
            break;
        case "coinflip":
            roll.flip(message);
            break;
        case "flipcoin":
            roll.flip(message);
            break;
        case "report":
            staff.sendReportForm(bot, message);
            break;
        case "bday":
            bday.writeBday(message, pool)
            break;
        case "date":
            dt.getDate();
            break;
        case "time":
            dt.getTime();
            break;
        case "event":
            site.route(message, site_pool);
            break;
        case "events":
            site.route(message, site_pool);
            break;
        default:
            if(checkStaff(message.member)) {
                staff.adminRespond(bot, message, pool, site_pool);
            }
            break;
    }
}

function DMrespond(bot, message, pool) { // internal
    let input = message.content.toLowerCase().slice(1, message.content.length);
    switch (input.split(" ")[0]) {
        case "report":
            staff.sendReportForm(bot, message);
            break;
        case "eid":
            let user_id = message.author.id;
            GoG = bot.guilds.find(x => x.name === 'Guild of Geeks');
            let member = GoG.members.find(x => x.id === user_id);
            if(checkStaff(member)) {
                eid.route(bot, message, pool);
            }
            break;
        default:
            break;
    }
}

function trigger(bot, message) { // internal
    let input = message.content.toLowerCase();
    if(message.mentions.users.find(x => x.id == '256269727142248448')) {
        if(input.includes('hi') || input.includes('hey') || input.includes('hello')) {
            message.reply('hello!');
        }
        if(input.includes("i love you") && message.author.id == '227607583106662400') {
            message.reply('I love you too!')
        }
    }
    else {
        if(message.channel.type === 'dm') {
            if(input.includes('report') || input.includes('complain')
            || input.includes('harass') || input.includes('problem')) {
                staff.sendReportForm(bot, message);
            }
        }
    }
}

function helpList(message) { // internal
    let list = `Available commands are:\n\n`
    + `\`\`!role ROLE\`\` = Adds/removes a role assigned to you.\n`
    + `\`\`!report\`\` = Privately sends you the link to the Google Form for `
        + `submitting a complaint or report. You can DM me this command, too.\n`
    + `\`\`!roll XdY modZ\`\` = Rolls X number of dice with Y number of sides. `
        + `A mod is not required.\n`
    + `\`\`!coinflip\`\` or \`\`!flipcoin\`\` = Flips a coin.\n`
    + `\`\`!bday MM-DD\`\` = Enters your birthday into \"the system\" so I can `
        + `wish you a happy birthday on your birthday.\n`
    + `\n**To access Groovy Bot, please type \`\`!help\`\` in the `
        + `<#582449514758275082> channel.**`
    list += adminHelpList(message);
    message.channel.send(list);
}

function adminHelpList(message) { // internal
    if(message.channel.parent.name === 'Staff') {
        let staff_list = `\n\nAvailable staff commands are:\n\n`
        + `\`\`!users ROLE\`\` = Lists all users with a certain role.\n`
        + `\`\`!eid\`\` = I'll PM you with your GoG employee ID.\n`
        + `\`\`!site\`\` = I'll help you update events on the website.\n`
        + `\`\`!timeout @USER\`\` = Toggles a user's Time Out role.\n`
        + `\`\`!kick @USER - REASON\`\` = Kicks a user and logs the reason.\n`
        + `\`\`!ban @USER - REASON\`\` = Bans a user and logs the reason.`
        return staff_list;
    }
    else {
        return '';
    }
}

function filterGroovy(message) { // internal
    if(message.content.substring(0,1) !== "!") {
        message.delete()
        .then(message => message.channel.send(`<@${message.author.id}> Your message `
          + `was deleted because it was not a valid Groovy Bot command.\n`
          + `Please type \`\`!help\`\` to get a list of Groovy Bot commands.`))
        .catch(console.error);
    }
    else {
        let command = '';
        if(message.content.split(" ").length > 1) {
            command += `${message.content.split(" ")[0]}`;
        }
        else {
            command += `${message.content}`;
        }
        while(command.charAt(0) === '!') {
            command = command.substr(1).toLowerCase();
        }
        if(!groovy.includes(command)) {
            message.delete()
            .then(message => message.channel.send(`<@${message.author.id}> Your message `
            + `was deleted because it was not a valid Groovy Bot command.\n`
            + `Please type \`\`!help\`\` to get a list of Groovy Bot commands.`))
            .catch(console.error);
        }
    }
}

groovy = ['play', 'queue', 'next', 'skip', 'back', 'clear', 'jump', 'loop',
'lyrics', 'q', 'pause', 'resume', 'remove', 'remove range', 'reset', 'shuffle',
'song', 'fast forward', 'rewind', 'search', 'seek', 'stop', 'move', 'prefix',
'announce', 'perms', '24/7', 'bass boost', 'volume', 'speed', 'pitch', 'help',
'nightcore', 'vaporwave', 'reset effects', 'fair queue', 'saved queue load',
'saved queue create', 'saved queue delete', 'saved queue share', 'disconnect']

function checkStaff(member) { // internal
    if(member.roles.find(x => x.name === "Staff")) {
        return true;
    }
    else {
        return false;
    }
}

function checkBday(pool) { // export
    bday.checkBday(pool);
}

module.exports = { route, checkBday }