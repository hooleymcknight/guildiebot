const fs = require('fs');
const config = require('config.json');
const ConfigParser = require('configparser');
const mysql = require('mysql');

const conf = new ConfigParser();
conf.read('./keys/config.ini');
conf.sections();

let db = {
    connectionLimit: 5,
    host: conf.get('AWS', 'host'),
    port: conf.get('AWS', 'port'),
    user: conf.get('AWS', 'user'),
    password: conf.get('AWS', 'pw'),
    database: conf.get('AWS', 'db'),
    ssl: {
        ca: fs.readFileSync('./keys/rds-combined-ca-bundle.pem')
    }
};

let site_db = {
	connectionLimit: 5,
	host: conf.get('SITE', 'host'),
	port: conf.get('SITE', 'port'),
    user: conf.get('SITE', 'user'),
    password: conf.get('SITE', 'pw'),
    database: conf.get('SITE', 'db'),
}

function gbot_pool() {
	let pool = mysql.createPool(db);
	return pool;
}

function site_pool() {
	let site_pool = mysql.createPool(site_db);
	return site_pool;
}

function login() {
    return conf.get('DEFAULT', 'gbot');
}

module.exports = { login, gbot_pool, site_pool };