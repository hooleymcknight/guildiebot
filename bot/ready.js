function setChannels(bot) {
    GoG = bot.guilds.find(x => x.name === 'Guild of Geeks');

    WelcomeChannel = GoG.channels.find(x => x.name === 'welcome');
    GeneralChannel = GoG.channels.find(x => x.name === 'general');
    MusicChannel = GoG.channels.find(x => x.name === 'music-bot');

    RWBYChannel = GoG.channels.find(x => x.name === 'rwby-chat');
    UtenaChannel = GoG.channels.find(x => x.name === 'utena-chat');
    MHAChannel = GoG.channels.find(x => x.name === 'mha-chat');
    MangaChannel = GoG.channels.find(x => x.name === 'manga-spoilers');
    GenLOCKChannel = GoG.channels.find(x => x.name === 'gen-lock');
    RWBYSpoilersChannel = GoG.channels.find(x => x.name === 'rwby-spoilers');

    RTXChannel = GoG.channels.find(x => x.name === 'rtx');
    PAXChannel = GoG.channels.find(x => x.name === 'pax-south');
    AustinChannel = GoG.channels.find(x => x.name === 'austin-meetups');
    DFWChannel = GoG.channels.find(x => x.name === 'dfw-meetups');
    WacoChannel = GoG.channels.find(x => x.name === 'waco-meetups');
    TRFChannel = GoG.channels.find(x => x.name === 'trf');

    TestChannel = GoG.channels.find(x => x.name === 'hooley-test');

    StaffChannel = GoG.channels.find(x => x.name === 'staff-general');
    DirectorsChannel = GoG.channels.find(x => x.name === 'directors-discussion');
    ArtChannel = GoG.channels.find(x => x.name === 'art-board');
    EventsChannel = GoG.channels.find(x => x.name === 'events-room');
    FinanceChannel = GoG.channels.find(x => x.name === 'finance-exchange');
    MediaChannel = GoG.channels.find(x => x.name === 'social-media-feed');
    ModChannel = GoG.channels.find(x => x.name === 'moderation-chat');
    TechChannel = GoG.channels.find(x => x.name === 'tech-talk');
}

function setRoles(bot) {
    GoG = bot.guilds.find(x => x.name === 'Guild of Geeks');
    let names = GoG.roles.map(g => g.name);
    let roles_obj = {};
    for(i=0; i<names.length; i++) {
        let position = GoG.roles.map(g => g.position)[i];
        let name = names[i];
        roles_obj[position] = name;
    }

    Roles = [];
    let auto_roles = ['@everyone', 'Nitro Booster', 'Groovy'];
    for(i=0; i<names.length; i++) {
        if(auto_roles.indexOf(roles_obj[`${i}`]) == -1 && roles_obj[`${i}`]) {
            Roles.push(roles_obj[`${i}`]);
        }
    }

    RolesAddable = [];
    for(i=0; i<Roles.length; i++) {
        if(Roles.indexOf(Roles[i]) < Roles.indexOf('Time Out')) {
            RolesAddable.push(Roles[i])
        }
    }
}

function setEmojis() {
    Emojis = {
        'PAX South': '<:paxsouth:336948064113655808>',
        'RTX': '<:rtx:534501137668112390>',
        'Austin': '<:texas:354127874690777089>',
        'DFW': '<:texas:354127874690777089>',
        'Waco': '<:texas:354127874690777089>',
        'Overwatch': '<:overwatch:361027771989884929>',
        'gen:LOCK': '<:genlock:540392086516793344>',
        'RWBY': '<:ruby:296044361106325506>',
        'MHA': '<:mha:460518241132085256>',
        'Utena': '<:utena:337450286065123329>',
        'Manga Spoilers': '<:manga:507991221734473738>',
        'RWBY Spoilers': '<:rwbyspoilers:534503039420071947>',
        'RWBY Amity': '<:rwbyamity:534539868831875083>',
        'Super Smash Bros': '<:supersmashbros:534500694749609994>',
        'Minecraft': '<:minecraft:616888502545678349>'
    }
}

function userDB(message, pool) { // internal
    let all_users = message.guild.members.map(x => x.user);
        message.guild.members.forEach(function(member) {
            pool.query(`INSERT INTO users (user_id, username)
                VALUES ("${member.id}", "${member.user.username}")`, function(err) {
                    if(err) throw err;
                })
        })
}

function setUp(bot) {
    setEmojis();
    setChannels(bot);
    setRoles(bot);
}

module.exports = { setUp, setRoles, setChannels };