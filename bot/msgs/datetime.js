const {DateTime} = require('luxon');

function getDT() { // export
    let local = DateTime.local().setZone('America/Chicago').c;
    let date_time = processDate(local);
    return date_time;
}

function getDate() {
	let date = getDT().split(" ")[0];
	return date;
}

function getTime() {
	let time = getDT().split(" ")[1];
	return time;
}

function getWeekSpan() {
	let nextWeek = DateTime.local().plus({ days: 7 }).setZone('America/Chicago').c;
	let endDay = processDate(nextWeek).split(" ")[0];
	return endDay;
}

function processDate(local) { // internal
	let yyyy = local.year;
	let mm = frontZero(local.month);
	let dd = frontZero(local.day);
	let hh = frontZero(local.hour);
	let mi = frontZero(local.minute);
	let ss = frontZero(local.second);
	return `${yyyy}-${mm}-${dd} ${hh}:${mi}:${ss}`;
}

function frontZero(input) { // internal
	if(input < 10) {
		input = `0${input}`;
	}
	return input;
}

module.exports = { getDT, getDate, getTime, getWeekSpan }