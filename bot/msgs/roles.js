const ready = require('../ready');
const update = require('../update-member.js');

function updateRole(message, RolesAddable) { // export
    let [req, current_roles, available_roles] = processRole(message, RolesAddable);
    if(req) {
        let role = findRole(message, req, current_roles, available_roles);
        if(role) {
            toggleRole(message, role);
        }
    }
}

function processRole(message, RolesAddable) { // internal
    let [current_roles, available_roles] = getRoleSets(message, RolesAddable);
    if(message.content.split(" ").length < 2) {
        replyError(message, current_roles, available_roles);
        return [false, false, false];
    }
    else {
        let req = message.content.split(" ")[1];
        if(message.content.split(" ").length > 2) {
            req += ` ${message.content.split(" ")[2]}`;
        }
        return [req, current_roles, available_roles];
    }
}

function findRole(message, req, current_roles, available_roles) { // internal
    let role = message.guild.roles.find(x => x.name.toLowerCase() === req.toLowerCase());
    if(role) {
        return role;
    }
    else {
        replyError(message, current_roles, available_roles);
        return false;
    }
}

function toggleRole(message, role) { // internal
    let member = message.member;
    if(member.roles.find(x => x.name === role.name)) {
        member.removeRole(role);
        message.reply(`You have removed the ${role.name} role.`);
    }
    else {
        member.addRole(role);
        message.reply(`You have been given the ${role.name} role.`);
    }
}

function replyError(message, current_roles, available_roles) {
    if(current_roles.length == 0) {
        current_roles = ['None'];
    }
    message.reply(`To toggle a role on or off, please type `
    + `\`\`!role ROLE NAME\`\`. For example, \`\`\`!role MHA\`\`\``
    + `If you choose RWBY, Utena, MHA, Gen:Lock, or any Spoilers role, `
    + `please be prepared for the spoilers in those text channels.\n\n`
    + `Your current roles are:\`\`\`${current_roles.join(', ')}\`\`\``
    + `Available roles are:\`\`\`${available_roles.join(', ')}\`\`\``)
}

function getRoleSets(message, RolesAddable) { // internal
    let user = message.member;
    let guild = message.guild;
    let current_roles = removeAutoRoles(user.roles.map(x => x.name));
    let all_roles = removeAutoRoles(guild.roles.map(x => x.name));
    let available_roles = getDiff(current_roles, all_roles);
    let unavailable_roles = getDiff(RolesAddable, available_roles);
    available_roles = getDiff(unavailable_roles, available_roles);
    return [current_roles, available_roles];
}

function removeAutoRoles(array) { // internal
    let out_roles = [];
    let auto_roles = ['@everyone', 'Nitro Booster', 'Groovy'];
    for(i=0; i<array.length; i++) {
        if(!auto_roles.includes(array[i])) {
            out_roles.push(array[i])
        }
    }
    return out_roles;
}

function getDiff(current_roles, all_roles) { // internal
    let a = [], diff = [];
    for(i=0; i<current_roles.length; i++) {
        a[current_roles[i]] = true;
    }
    for(i=0; i<all_roles.length; i++) {
        if (a[all_roles[i]]) {
            delete a[all_roles[i]];
        } else {
            a[all_roles[i]] = true;
        }
    }
    for(k in a) {
        if(!current_roles.includes(k)) {
            diff.push(k);
        }
    }
    return diff;
}

module.exports = { updateRole };