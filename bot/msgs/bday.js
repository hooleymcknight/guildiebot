function checkFormat(user_id, bday, message) {
	err_msg = `<@${user_id}> Please type \`\`!bday MM-DD\`\``
		+ `with your birthday in the MM-DD format. For example:`
		+ `\`\`\`!bday 08-12\`\`\``
	if(typeof(bday) == 'undefined' || bday.length !== 5 || !bday.includes("-")) {
		message.channel.send(err_msg);
		return false;
	}
	else {
		return true;
	}
}

function writeBday(message, pool) {
	let user_id = message.member.id;
	let bday = message.content.split(" ")[1];
	if(checkFormat(user_id, bday, message) == true) {
		let db_bday = new Promise(function(resolve, reject) {
	        setTimeout(function() {
	            pool.query(`SELECT bday FROM users WHERE user_id = "${user_id}"`,
	                function(err, rows) {
	                    if(err) throw err;
	                    resolve(rows[0].bday);
	                })
	        }, 300);
	    });

	    db_bday.then(function(value) {
	        if(value == null) {
	            pool.query(`UPDATE users SET bday = "2019-${bday}"
	            	WHERE user_id = "${user_id}"`, function(err) {
	            		if(err) throw err;
	            	})
	            message.channel.send(`<@${user_id}> Your birthday has just been `
	            	+ `recorded as ${bday}. If this is incorrect, please `
	            	+ `contact the Technology Committee in <#481673797569609729> chat.`)
	        }
	        else {
	        	let result = fixFormat(value);
	            message.channel.send(`<@${user_id}> Your birthday has already been `
	                + `recorded as ${result}. If this is incorrect, `
	                + `please contact the Technology Committee in <#481673797569609729> chat.`)
	        }
	    });
	}
}

function fixFormat(d) {
	let mm = d.getMonth() + 1;
	if(mm < 10) {
		mm = '0' + mm;
	}
	let dd = d.getDate();
	if(dd < 10) {
		dd = '0' + dd;
	}
	return `${mm}-${dd}`;
}

function checkBday(pool) {
	let d = new Date();
	let yyyy = '2019';
	let mmdd = fixFormat(d);
	let today = `${yyyy}-${mmdd}`;
	let current_bday = new Promise(function(resolve, reject) {
        setTimeout(function() {
            pool.query(`SELECT user_id FROM users WHERE bday = "${today}"`, function(err, rows) {
				if(err) throw err;
				if(rows.length > 0) {
					resolve(rows.map(x => x.user_id))
				}
				else { resolve(false); }
			})
        }, 300);
    });

	current_bday.then(function(value) {
		if(value !== false) {
			if(value.length == 1) {
				GeneralChannel.send(`Happy birthday to <@${value[0]}>!`)
			}
			else {
				let birthdays = [];
				for(i=0; i<value.length; i++) {
					birthdays.push(`<@${value[i]}>`)
				}
				birthdays = birthdays.join(' and ')
				GeneralChannel.send(`Happy birthday to ${birthdays}!`)
			}
		}
	})
}

module.exports = { writeBday, checkBday }