function doRoll(message) {
	if(message.content.split(" ").length < 2 || !message.content.includes("d")) {
		message.channel.send(`To roll a die, please use the following format: `
		+ `\`\`\`!roll 2d6\`\`\` or \`\`\`!roll d20 mod5\`\`\``);
		return;
	}
	let dice = message.content.split(" ")[1].split("d")[0];
	if(dice.length == 0) {
		dice = '1';
	}
	dice = Number(dice);
	if(dice < 1 || dice > 100) {
		message.channel.send(`You can only roll between 1 and 100 dice at once.`);
		return;
	}
	else {
		let sides = message.content.split(" ")[1].split("d")[1];
		sides = Number(sides);
		if(sides < 3 || sides > 100) {
			message.channel.send(`Your dice must have between 3 and 100 sides.`);
			return;
		}
		else {
			let mod = 0;
			if(message.content.split("mod").length > 1) {
				mod = message.content.split("mod")[1];
				mod = Number(mod);
			}
			let result = rollDice(dice, sides, mod);
			message.channel.send(`${message.author} ${result}`)
		}
	}
}

function roll(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function rollDice(dice, sides, mod) {
	let roll_results = [];
	let total = 0;
	for(i=0; i<dice; i++) {
		let single_roll = roll(1, sides);
		roll_results.push(single_roll);
		total += single_roll;
	}

	let result = `\`\`\`Rolls:\n` + roll_results.join('\n');
	if(mod > 0) {
		result += `\nMod: ${mod}\nResult: ${total + mod}\`\`\``;
	}
	else {
		result += `\nResult: ${total}\`\`\``;
	}
	return result;
}

function flip(message) {
	let result = roll(1, 2)
	if(result == 1) {
		message.channel.send(`${message.author} Heads.`)
	}
	else {
		message.channel.send(`${message.author} Tails.`)
	}
}

module.exports = { doRoll, flip };