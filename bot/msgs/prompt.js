const prompter = require('discordjs-prompter');

function nextPrompt(msg, info, i, out_info, type, _callback) { // internal
	if(type == 'edit' && i == 3) {
		if(fieldValid(out_info[2])) {
			info[i].question = `${getEditQuestion(out_info[2])}`;
			info[i].request = `Setting ${out_info[2]} to`;
		}
		else {
			msg.reply(`That is not a valid edit field. Please try again.`);
			return;
		}
	}
	prompter.message(msg.channel, {
		question: info[i].question,
		userId: msg.author.id,
		max: 1,
		timeout: 20000,
	})
	.then(responses => {
		if(!responses.size) {
			return msg.channel.send(`Your request timed out. Please try again.`);
		}
		const response = responses.first().content;
		msg.channel.send(`${info[i].request}: ${response}`);
		out_info.push(response);

		if(info[i].final==false) {
			let k = i + 1;
			nextPrompt(msg, info, k, out_info, type, _callback);
		}
		else {
			_callback();
		}
	});
}

function fieldValid(field) { // internal
	let fields = ['name', 'date', 'time', 'location', 'address'];
	if(fields.includes(field)) {
		return true;
	}
	else {
		return false;
	}
}

function getEditQuestion(field) { // internal
	let question = '';
	switch (field) {
		case "name":
			question += `What would you like to change the name to?`;
			return question;
			break;
		case "date":
			question += `What would you like to change the date to? `
			+ `Please use the following format:\`\`\`YYYY-MM-DD\`\`\``;
			return question;
			break;
		case "time":
			question += `What would you like to change the time to? `
			+ `Please use the following format:\`\`\`9:00pm\`\`\``;
			return question;
			break;
		case "location":
			question += `What would you like to change the location to? `
			+ `Examples:\`\`\`Oilcan Harry's\nInfo on Request\`\`\``;
			return question;
			break;
		case "address":
			question += `What would you like to change the address to? `
			+ `Please use the following format:\`\`\`211 W 4th St, Austin, TX 78701\`\`\``;
			return question;
			break;
	}
}

module.exports = { nextPrompt }