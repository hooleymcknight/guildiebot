const Discord = require('discord.js');
const prompt = require('../prompt');

function createEvent(msg, site_pool) { // export
	let out_info = [];
	prompt.nextPrompt(msg, site_create_info, 0, out_info, 'create', function() {
		let event = out_info[0];
		let date = out_info[1];
		let time = out_info[2];
		let location = out_info[3];
		let address = out_info[4];
		if(out_info[4].toLowerCase() == "n/a") {
			site_pool.query(`INSERT INTO austin_events (name, date, time, location)
				VALUES ("${event}", "${date}", "${time}", "${location}")`,
				function(err, results) {
					if(err || results.affectedRows == 0) {
						msg.reply(`Something went wrong. Please check your date format and try again.`)
					}
					else {
						msg.channel.send(`Your event has been created with the following information:\n\n`
						+ `Event name: ${event}\nDate: ${date}\nTime: ${time}\n`
						+ `Location: ${location}\nAddress: ${address}`)
					}
				});
		}
		else {
			site_pool.query(`INSERT INTO austin_events (name, date, time, location, address)
				VALUES ("${event}", "${date}", "${time}", "${location}", "${address}")`,
				function(err, results) {
					if(err || results.affectedRows == 0) {
						msg.reply(`Something went wrong. Please check your date format and try again.`)
					}
					else {
						msg.channel.send(`Your event has been created with the following information:\n\n`
						+ `Event name: ${event}\nDate: ${date}\nTime: ${time}\n`
						+ `Location: ${location}\nAddress: ${address}`)
					}
				});
		}
	})
}

function editEvent(msg, site_pool) { // export
	let out_info = [];
	prompt.nextPrompt(msg, site_edit_info, 0, out_info, 'edit', function() {
		let current_name = out_info[0];
		let current_date = out_info[1];
		let edit_field = out_info[2];
		let edit_result = out_info[3];
		site_pool.query(`UPDATE austin_events SET ${edit_field}="${edit_result}"
			WHERE name="${current_name}" AND date="${current_date}"`,
			function(err, results) {
				if(err || results.affectedRows == 0) {
					msg.reply(`I couldn't find an event with that name and date. `
					+ `Please try again.`);
				}
				else {
					msg.channel.send(`Your event has been updated with the following information:\n\n`
					+ `Original event name: ${current_name}\nOriginal date: ${current_date}\n\n`
					+ `**Edit:**\n${edit_field}: ${edit_result}`);
				}
			})
	})
}

function deleteEvent(msg, site_pool) { // export
	let out_info = [];
	prompt.nextPrompt(msg, site_delete_info, 0, out_info, 'delete', function() {
		let name = out_info[0];
		let date = out_info[1];
		let sure = isSure(out_info[2]);
		if(sure == 0) {
			msg.reply(`Okay, I won't delete anything just yet.`)
		}
		else if(sure == 1) {
			site_pool.query(`DELETE FROM austin_events
				WHERE name = "${name}" AND date = "${date}"`,
				function(err, results) {
					if(err || results.affectedRows == 0) {
						msg.reply(`I couldn't find an event with that name on that day. `
						+ `Please try again.`);
					} else {
						msg.channel.send(`The following event has been removed from the website:\n\n`
						+ `${name}\n${date}`);
					}
				})
		}
		else {
			msg.reply(`Please try again.\nhttps://www.tenor.co/oLrl.gif`)
		}
	})
}

function isSure(sure) { // internal
	if(yay.includes(sure)) {
		return 1;
	}
	else if(nay.includes(sure)) {
		return 0;
	}
	else {
		return 3;
	}
}

yay = ['yes', 'yeah', 'yea', 'y', 'yee', 'yep', 'yup', 'yus', 'yay', 'affirmative', 'sure', 'positive'] // internal
nay = ['no', 'nah', 'na', 'n', 'nope', 'negative', 'negatory', 'not', 'non', 'unsure'] // internal

site_create_info = [ // internal
	{
		"question": "If you need to quit this process, just ignore me.\n\nWhat is the event name?",
		"request": "Event name",
		"final": false
	},
	{
		"question": "What date is the event? Please use the following format:\`\`\`YYYY-MM-DD\`\`\`",
		"request": "Event date",
		"final": false
	},
	{
		"question": "What time is the event? Please use the following format:\`\`\`8:00pm\`\`\`",
		"request": "Event time",
		"final": false
	},
	{
		"question": "What is the location of the event? If online, you may say Twitch or Facebook Live. "
		+ "Otherwise, please name the venue. Examples: \`\`\`Oilcan Harry's\nHolly's House\`\`\`",
		"request": "Event location",
		"final": false
	},
	{
		"question": "What is the address of the event? If none or at someone's house, say \`\`N/A\`\`. "
		+ "Otherwise, type in the address in the following format:\`\`\`211 W 4th St, Austin, TX 78701\`\`\`",
		"request": "Event address",
		"final": true
	}
]

site_edit_info = [ // internal
	{
		"question": "If you need to quit this process, just ignore me.\n\nWhat is the current name of the event you'd like to edit?",
		"request": "Current event name",
		"final": false
	},
	{
		"question": "What is the current date of the event you'd like to edit? Please use the following format:\`\`\`YYYY-MM-DD\`\`\`",
		"request": "Current event date",
		"final": false
	},
	{
		"question": "What field would you like to edit? Options:\`\`\` name\n date\n time\n location\n address\`\`\`",
		"request": "Field to edit",
		"final": false
	},
	{
		"question": ``,
		"request": ``,
		"final": true
	}
]

site_delete_info = [ // internal
	{
		"question": "If you need to quit this process, just ignore me.\n\nWhat is the name of event you'd like to delete?",
		"request": "Event name",
		"final": false
	},
	{
		"question": "What is the date of the event you'd like to delete?",
		"request": "Event date",
		"final": false
	},
	{
		"question": "Are you super sure you want to delete this event?",
		"request": "Your level of sureness",
		"final": true
	}
]

module.exports = { createEvent, editEvent, deleteEvent }