const dt = require('../datetime');
const eid = require('./eid');
const site = require('./site');
const reminder = require('./reminder');

function adminRespond(bot, message, pool, site_pool) { // export
    let input = message.content.toLowerCase().slice(1, message.content.length);
    switch (input.split(" ")[0]) {
    	case "users":
    		listUsers(message);
    		break;
    	case "eid":
    		eid.route(bot, message, pool);
    		break;
    	case "site":
    		site.adminRoute(message, site_pool);
    		break;
        case "share":
            share(message);
            break;
    	case "reminder":
    		reminder.route(bot, message, pool);
    		break;
        case "timeout":
            timeoutUser(message);
            break;
		case "kick":
			kickUser(message, pool);
			break;
		case "ban":
			banUser(message, pool);
			break;
		default:
			break;
	}
}

function listUsers(message) { // internal
	if(message.content.split(" ").length < 2) {
		message.reply(`To list the users in a role, please type \`\`!users ROLE\`\``);
	}
	else {
		let req = message.content.split(" ")[1];
		if(message.content.split(" ").length > 2) {
			for(i=2; i<message.content.split(" ").length; i++) {
				req += ` ${message.content.split(" ")[i]}`;
			}
		}
		let role = message.guild.roles.find(x => x.name.toLowerCase() === req.toLowerCase());
		if(!role) {
			message.reply(`That role does not exist. Please try again.`);
			return;
		}
		else {
			let members = role.members.map(x => x.user.username);
			message.reply(`Users with that role are:\n${members.join("\n")}`)
		}
	}
}

function share(message) { // internal
    if(message.content.split(" ").length < 3) {
        message.reply(`To share a message, please mention a channel and follow `
        + `it with your message. For example:\`\`\`#event-announcements Check `
        + `out this YouTube highlight!\`\`\``)
    }
    else {
        let channel = trimEnd(message.content.split(">")[0]).split("!share <#")[1];
        channel = message.guild.channels.find(x => x.id === channel)
        let req = trimStart(message.content.split(">")[1]);
        channel.send(req);
        message.reply(`Your message has been sent in ${channel}:\n${req}`);
    }
}

function timeoutUser(message) { // internal
	if(message.content.split(" ").length < 2) {
		message.reply(`To toggle a timeout, please type \`\`!timeout @USER\`\``)
	}
	else {
		let role = message.guild.roles.find(x => x.name === 'Time Out');
		let user_id = message.content.split(" ")[1];
		user_id = user_id.replace('<@', '').replace('>', '');
		let member = message.guild.fetchMember(user_id)
		.then(member => {
			if(member.roles.find(x => x.name === 'Time Out')) {
				member.removeRole(role);
				message.reply(`The following user has been freed from Time Out: `
				+ `<@${user_id}>`)
				member.send(`Hello again! Your timeout has been lifted. See `
				+ `you back in the server!`)
			}
			else {
				member.addRole(role);
				message.reply(`The following user has been placed in Time Out: `
				+ `<@${user_id}>`)
				member.send(`You have been placed in a temporary time out by `
				+ `a GoG Staff member. When you are in time out, you cannot `
				+ `type in GoG text channels or connect to GoG voice `
				+ `channels. I'll message you again to let you know when `
				+ `the timeout has been lifted. Please remember that we `
				+ `still love you! Talk to you soon, friend.`)
			}
		}).catch(console.error);
	}
}

function kickUser(message, pool) {
	if(message.content.split("-").length < 2) {
		message.reply(`To kick a user, please type \`\`!kick @USER - Reason `
		+ `described after a dash\`\``)
	}
	else {
		let user_id = message.content.split(" ")[1];
		user_id = user_id.replace('<@', '').replace('>', '');
		let reason = trimStart(message.content.split("-")[1]);
		let date_time = dt.getDT();
		let member = message.guild.fetchMember(user_id)
		.then(member => {
			member.send(`You are being kicked from the Guild of Geeks server `
			+ `for the following reason: \`\`\`${reason}\`\`\``
			+ `We are very serious about keeping our community a fun and safe `
			+ `environment for all. Should you choose to return, please do `
			+ `not continue the aforementioned behavior. Thank you!`)
			.then(() => {
				member.kick(reason)
				pool.query(`INSERT INTO server_kicks (user_id, reason, date_time)
					VALUES ("${user_id}", "${reason}", "${date_time}")`)
				ModChannel.send(`<@&253711221465808896> The user <@${user_id}> has `
					+ `been kicked for the following reason: \`\`\`${reason}\`\`\``)
			})
		}).catch((err) => {
			console.log(err)
			message.reply(`To kick a user, please type \`\`!kick @USER - Reason `
			+ `described after a dash\`\`. Please be sure to get the username `
			+ `tagged correctly so that I can find the user who needs kicked.`)
		});
	}
}

function banUser(message, pool) {
	if(message.content.split("-").length < 2) {
		message.reply(`To ban a user, please type \`\`!ban @USER - Reason `
		+ `described after a dash\`\``)
	}
	else {
		let user_id = message.content.split(" ")[1];
		user_id = user_id.replace('<@', '').replace('>', '');
		let reason = trimStart(message.content.split("-")[1]);
		let date_time = dt.getDT();
		let member = message.guild.fetchMember(user_id)
		.then(member => {
			member.send(`You are being permanently banned from the Guild of `
			+ `Geeks server for the following reason: \`\`\`${reason}\`\`\``
			+ `We are very serious about keeping our community a fun and safe `
			+ `environment for all. We wish you the best in your endeavors `
			+ `outside the Guild of Geeks.`)
			.then(() => {
				member.ban(reason)
				pool.query(`INSERT INTO server_bans (user_id, reason, date_time)
					VALUES ("${user_id}", "${reason}", "${date_time}")`)
				ModChannel.send(`<@&253711221465808896> The user <@${user_id}> has `
					+ `been banned for the following reason: \`\`\`${reason}\`\`\``)
			})
		}).catch((err) => {
			console.log(err)
			message.reply(`To ban a user, please type \`\`!ban @USER - Reason `
			+ `described after a dash\`\`. Please be sure to get the username `
			+ `tagged correctly so that I can find the user who needs banned.`)
		});
	}
}

function trimStart(input) { // internal
	while(input.charAt(0) === ' ') {
		input = input.substr(1);
	}
	return input;
}

function trimEnd(input) { // internal
    let end = input.length - 1;
    while(input.charAt(end) === ' ') {
        input = input.substr(0, end);
        end -= 1;
    }
    return input;
}

function sendReportForm(bot, message) { // export
	let user_id = message.author.id;
	let user = bot.fetchUser(user_id)
    .then(user => {
		user.send(`Here is the form to submit an online complaint or report `
		+ `regarding the Guild of Geeks community or Discord server: `
		+ `https://forms.gle/ajfrCmfmWU25Q6X9A`)
		.catch(function(err) {
			console.log(err)
		})
    }).catch(console.error);
}

module.exports = { adminRespond, sendReportForm }