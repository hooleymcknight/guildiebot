const dt = require('../datetime');
const adminSite = require('./admin_site');

function route(message, site_pool) { // export
	if(message.content.split(" ").length == 1) {
		showNextThree(message, site_pool);
	}
	else {
		let input = message.content.toLowerCase().split(" ")[1];
		switch (input) {
			case "atx":
				showNextThreeATX(message, site_pool);
				break;
			case "sunday":
				getEventsByDay(message, site_pool, 0);
				break;
			case "monday":
				getEventsByDay(message, site_pool, 1);
				break;
			case "tuesday":
				getEventsByDay(message, site_pool, 2);
				break;
			case "wednesday":
				getEventsByDay(message, site_pool, 3);
				break;
			case "thursday":
				getEventsByDay(message, site_pool, 4);
				break;
			case "friday":
				getEventsByDay(message, site_pool, 5);
				break;
			case "saturday":
				getEventsByDay(message, site_pool, 6);
				break;
			default:
				showNextThree(message, site_pool);
				break;
		}
	}
}

function adminRoute(message, site_pool) { // export
	// create, edit, delete events
	if(message.content.split(" ").length == 1) {
		// correct the user
		message.reply(`You can create, edit, or delete events from `
			+ `the website by typing \`\`!site create\`\` or `
			+ `\`\`!site edit\`\` or \`\`!site delete\`\`.`)
	}
	else {
		switch (message.content.split(" ")[1]) {
			case "create":
				adminSite.createEvent(message, site_pool);
				break;
			case "edit":
				adminSite.editEvent(message, site_pool);
				break;
			case "delete":
				adminSite.deleteEvent(message, site_pool);
				break;
			default:
				message.reply(`You can create, edit, or delete events from `
					+ `the website by typing \`\`!site create\`\` or `
					+ `\`\`!site edit\`\` or \`\`!site delete\`\`.`)
				break;
		}
	}
}

function showNextThree(message, site_pool) { // internal
	let today = dt.getDate();
	site_pool.query(`SELECT * FROM austin_events
		WHERE date >= "${today}" ORDER BY date LIMIT 3`,
		function(err, rows) {
			if(err) throw err;
			if(rows.length == 0) {
				message.reply(`There are no events posted yet within the next week. Please check `
					+ `the GoG website for more upcoming events. https://guildofgeeks.net/events/`)
			}
			else {
				events = [];
				for(i=0; i<rows.length; i++) {
					let clean_date = cleanDate(rows[i].date);
					let info = `${rows[i].name}\n${clean_date}, ${rows[i].time}\n${rows[i].location}`
					if(rows[i].address !== null) {
						info += `\n${rows[i].address}`
					}
					events.push(info);
				}
				message.reply(`The following events are coming up soon:\n${events.join('\n\n')}`);
			}
		})
}

function showNextThreeATX(message, site_pool) { // internal
	let today = dt.getDate();
	site_pool.query(`SELECT * FROM austin_events
		WHERE date >= "${today}" AND address IS NOT NULL
		ORDER BY date LIMIT 3`,
		function(err, rows) {
			if(err) throw err;
			if(rows.length == 0) {
				message.reply(`There are no events posted yet within the next week. Please check `
					+ `the GoG website for more upcoming events. https://guildofgeeks.net/events/`)
			}
			else {
				events = [];
				for(i=0; i<rows.length; i++) {
					let clean_date = cleanDate(rows[i].date);
					let info = `${rows[i].name}\n${clean_date}, ${rows[i].time}\n${rows[i].location}`
					if(rows[i].address !== null) {
						info += `\n${rows[i].address}`
					}
					events.push(info);
				}
				message.reply(`The following events are coming up soon:\n${events.join('\n\n')}`);
			}
		})
}

function getEventsByDay(message, site_pool, day) { // internal
	let today = dt.getDate();
	let week = dt.getWeekSpan();
	site_pool.query(`SELECT * FROM austin_events
		WHERE date >= "${today}" AND date <= "${week}"`,
		function(err, rows) {
			if(err) throw err;
			if(rows.length == 0) {
				message.reply(`There are no events on that day within the next week. Please check `
					+ `the GoG website for more upcoming events. https://guildofgeeks.net/events/`)
			}
			else {
				day_events = [];
				for(i=0; i<rows.length; i++) {
					if(day == findDay(rows[i].date)) {
						let clean_date = cleanDate(rows[i].date);
						let info = `${rows[i].name}\n${clean_date}, ${rows[i].time}\n${rows[i].location}`
						if(rows[i].address !== null) {
							info += `\n${rows[i].address}`
						}
						day_events.push(info);
					}
				}
				if(day_events.length > 0) {
					message.reply(`The following events are coming up this week:\n${day_events.join('\n\n')}`);
				}
				else {
					message.reply(`There are no events on that day within the next week. Please check `
						+ `the GoG website for more upcoming events. https://guildofgeeks.net/events/`);
				}
			}
		})
}

function findDay(date) { // internal
	let day = (new Date(date)).getDay();
	return day;
}

function cleanDate(date) {
	let month = date.toLocaleString('default', { month: 'long' });
	let day = date.getDate();
	let clean_date = `${month} ${day}`
	return clean_date;
}

module.exports = { route, adminRoute }