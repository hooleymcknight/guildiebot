function route(bot, message, pool) { // export
    if(message.author.id == '227607583106662400' && message.channel == TestChannel) {
        generateEID(bot, message, pool);
    }
    else {
        getEID(bot, message, pool);
    }
}

function getEID(bot, message, pool) { // internal
    let user_id = message.author.id;
    pool.query(`SELECT eid FROM employees WHERE user_id = "${user_id}"`,
        function(err, rows) {
            if(err) throw err;
            let user = bot.fetchUser(user_id)
            .then(user => {
                if(rows.length == 0) {
                    user.send(`An EID has not been created for you yet. `
                        + `Please contact a Director.`)
                }
                else {
                    let eid = rows[0].eid;
                    user.send(`Your EID for the Guild of Geeks is ${eid}.`)
                }
            }).catch(console.error);
        })
}

function getInfo(message) { // internal
    let fname = message.content.split(" ")[1];
    let lname = message.content.split(" ")[2];
    let user_id = message.content.split(" ")[3];
    let dob = message.content.split(" ")[4];
    user_id = user_id.replace("<@", "").replace(">", "");
    if(user_id.includes("!")) {
        user_id = user_id.replace("!", "");
    }
    return [fname, lname, user_id, dob];
}

function generateEID(bot, message, pool) { // internal
    if(message.content.split(" ").length < 5) {
        TestChannel.send("\`\`\`!eid Fname Lname @user_id DOB\`\`\`")
        return;
    }
    let info = getInfo(message);
    let eid = '';
    while(eid.length < 5) {
        let digit = Math.floor(Math.random() * 10);
        eid += digit;
    }
    pool.query(`INSERT INTO employees (fname, lname, eid, user_id, dob)
        VALUES ("${info[0]}", "${info[1]}", "${eid}", "${info[2]}", "${info[3]}")`, function(err) {
            if(err) throw err;
        })
    let user = bot.fetchUser(info[2])
    .then(user => {
        user.send(eid);
        TestChannel.send(`<@${user.id}> - ${eid}`);
    }).catch(console.error);
}

module.exports = { route }