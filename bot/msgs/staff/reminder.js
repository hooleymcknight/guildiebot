const dt = require('../datetime');

function route(bot, message, pool) { // export
	if(message.content.split(" ").length < 2) {
		message.reply(`To set up a reminder, please type\`\`!reminder `
		+ `create.\`\` To view existing ones, type \`\`!reminder view\`\`.`)
	}
	else {
		let input = message.content.split(" ")[1];
		switch (input) {
			case "create":
				createReminder();
				break;
			case "view":
				viewReminder();
				break;
			case "delete":
				deleteReminder();
			default:
				message.reply(`To set up a reminder, please type\`\`!reminder `
				+ `create.\`\` To view existing ones, type \`\`!reminder view\`\`.`)
				break;
		}
	}
}

function cleanDatabase(pool) {
	let today = dt.getDate();
	pool.query(`DELETE FROM reminders WHERE date < "${today}"`);
}

function createReminder(bot, message, pool) { // internal
	let out_info = [];
	nextPrompt(message, rem_create_info, 0, out_info, 'create', function() {
		let user_id = message.author.id;
		let cron_string = processToCron(out_info[0], out_info[1]);
		let reminder = out_info[2];
		pool.query(`INSERT INTO reminders (cron, message, user_id, date_created)
			VALUES ("${cron_string}", "${message}", "${user_id}", "${dt.getDate()}")`)
	})
}

function viewReminder(bot, message, pool) {
	let user_id = message.author.id;
	pool.query(`SELECT cron, message FROM reminders WHERE user_id = "${user_id}"`,
		function (err, rows) {
			if(rows.length == 0) {
				// reply no reminders
			}
			else {
				// reply with each reminder info
				let reminders = [];
				for(i=0; i<rows.length; i++) {
					let reminder = rows[i].cron
				}
			}
		})
}

function processToCron(date, time) { // internal
	let day = new Date(date).getDay();
	let hour = time.split(":")[0];
	let minute = time.split(":")[1];
	let cron_string = (`${minute} ${hour} * * ${day} *`);
	return cron_string;
}

function processFromCron(cron_string) { // internal
	let minute = cron_string.split(" ")[0];
	let hour = cron_string.split(" ")[1];
	let day = cron_string.split(" ")[4];
}

rem_create_info = [
	{
		"question": "What day do you want to be reminded? Please use the "
			+ "following date format: \`\`\`YYYY-MM-DD\`\`\`",
		"request": "Date",
		"final": false
	},
	{
		"question": "What time do you want to be reminded? Please use the "
			+ "following time format: \`\`\`8:00pm\`\`\`",
		"request": "Time",
		"final": false
	},
	{
		"question": "What do you want me to tell you?",
		"request": "Reminder message",
		"final": true
	}
]

module.exports = { route }