function findDiff(old_roles, new_roles) {
    let a = [], diff = [];
    for(i=0; i<old_roles.length; i++) {
        a[old_roles[i]] = true;
    }
    for(i=0; i<new_roles.length; i++) {
        if (a[new_roles[i]]) {
            delete a[new_roles[i]];
        } else {
            a[new_roles[i]] = true;
        }
    }
    for(k in a) {
        if(!old_roles.includes(k)) {
            diff.push(k);
        }
    }
    return diff;
}

function newRole(bot, oldMember, newMember) {
    let old_roles = Array.from(oldMember.roles.map(g => g.name));
    let new_roles = Array.from(newMember.roles.map(g => g.name));
    let new_msg_roles = findDiff(old_roles, new_roles);
    new_msg_roles.forEach(function(element) {
        switch (element) {
            case "Moderation Committee":
                ModChannel.send(`<@${newMember.id}> ${welcome_messages["Moderation Committee"]}`)
                break;
            case "Events Committee":
                EventsChannel.send(`<@${newMember.id}> ${welcome_messages["Events Committee"]}`)
                break;
            case "Finance Committee":
                FinanceChannel.send(`<@${newMember.id}> ${welcome_messages["Finance Committee"]}`)
                break;
            case "Social Media Committee":
                MediaChannel.send(`<@${newMember.id}> ${welcome_messages["Social Media Committee"]}`)
                break;
            case "Technology Committee":
                TechChannel.send(`<@${newMember.id}> ${welcome_messages["Technology Committee"]}`)
                break;
            case "Art Committee":
                ArtChannel.send(`<@${newMember.id}> ${welcome_messages["Art Committee"]}`)
                break;
            case "Staff":
                StaffChannel.send(`<@${newMember.id}> ${welcome_messages["Staff"]}`)
                break;
            case "PAX South":
                PAXChannel.send(`<@${newMember.id}> ${welcome_messages["PAX South"]}`)
                break;
            case "RTX":
                RTXChannel.send(`<@${newMember.id}> ${welcome_messages["RTX"]}`)
                break;
            case "TRF":
                TRFChannel.send(`<@${newMember.id}> ${welcome_messages["TRF"]}`)
                break;
            case "Austin":
                AustinChannel.send(`<@${newMember.id}> ${welcome_messages["Austin"]}`)
                break;
            case "DFW":
                DFWChannel.send(`<@${newMember.id}> ${welcome_messages["DFW"]}`)
                break;
            case "Waco":
                WacoChannel.send(`<@${newMember.id}> ${welcome_messages["Waco"]}`)
                break;
            case "MHA":
                MHAChannel.send(`<@${newMember.id}> ${welcome_messages["MHA"]}`)
                break;
            case "RWBY":
                RWBYChannel.send(`<@${newMember.id}> ${welcome_messages["RWBY"]}`)
                break;
            case "Utena":
                UtenaChannel.send(`<@${newMember.id}> ${welcome_messages["Utena"]}`)
                break;
            case "gen:LOCK":
                GenLOCKChannel.send(`<@${newMember.id}> ${welcome_messages["gen:LOCK"]}`)
                break;
            case "Manga Spoilers":
                MangaChannel.send(`<@${newMember.id}> ${welcome_messages["Manga Spoilers"]}`)
                break;
            case "RWBY Spoilers":
                RWBYSpoilersChannel.send(`<@${newMember.id}> ${welcome_messages["RWBY Spoilers"]}`)
                break;
            default:
                break;
        }
    })
}

welcome_messages = {
    "Moderation Committee": "Welcome to the Moderation Committee! Whenever you "
        + "do any Moderator duties, make sure to document everything you do "
        + "here in this channel.\n\nAlso, check out the pinned messages to see "
        + "the canned responses spreadsheet, and keep an eye out for messages "
        + "you get tagged in. Check out the pinned messages regularly to see "
        + "if there are any active votes going on that need your attention.",
    "Events Committee": "Welcome to the Events Committee! We have 3 sub-"
        + "committees here: Twitch, Online Anime, & Austin. You can "
        + "participate in any that you are interested in.\n\nCheck the pinned "
        + "messages for passwords, schedules, guides, and other resources. "
        + "Please do your best to keep up with messages you or your committee "
        + "gets tagged in.\n\nThanks for being here; we're excited to have you!",
    "Finance Committee": "Welcome to the Finance Committee! We're happy you're "
        + "here. Please check the pinned messages for passwords and the budget "
        + "document, and other important resources.",
    "Social Media Committee": "Welcome to the Social Media Committee! We're "
        + "glad to have you! In here, we'll coordinate advertising over our "
        + "various social media platforms. Ideas on who to follow/friend, what "
        + "to post, and other various social media activities to participate "
        + "in can be posted here.\n\nIn the pinned messages, you'll see links "
        + "to our media platforms and other guides to connecting and working "
        + "with our social media. If you have questions, please ask away!",
    "Technology Committee": "Welcome to the Technology Committee! We're "
        + "excited to have you on the tech team. Feel free to share any code "
        + "ideas you have here, regardless of whether or not you think you're "
        + "able to execute it. We can all learn together here.\n\nPlease be "
        + "very careful with passwords, SSH keys, and any other sensitive "
        + "information. Use the test server freely! But keep in mind that "
        + "no one's work will be sent to the live servers without being tested "
        + "and code reviewed by others and the Tech lead.\n\nLet's work "
        + "together and remember to tip your Guildie Bot! (I cannot actually "
        + "acquire tips, as I'm only a bot. But the thought is appreciated.) "
        + "Check the pinned messages for access to servers and guides to get "
        + "you set up.",
    "Art Committee": "Welcome to the Art Committee! We're happy to have you on "
        + "board. In here, feel free to post any screenshots, critiques, "
        + "project hold-ups, or really just anything art-related that would be "
        + "of benefit to the team.\n\nIn the pinned messages, you'll see a "
        + "bounty list of projects that you can freely add to or claim.\n\n"
        + "Just make sure to update Trello with what projects you want to pick "
        + "up, or add new projects you think would be cool! Be sure to touch "
        + "base here before you start something new. Have fun!",
    "Staff": "Welcome to Guild of Geeks staff! We're happy to have you on the "
        + "team. Consider me your lovely assistant for staff-related duties. "
        + "Just type \`\`!help\`\` in a staff channel to get your extended "
        + "commands list.\n\nPlease make sure to regularly check in with these "
        + "staff channels, but don't feel like you have to read everything. "
        + "Mostly just keep an eye out for things you or your committee role "
        + "get tagged in.\n\nPlease feel free to ask any questions at any time!",
    "PAX South": "Welcome to the PAX South chat! Please see pinned messages "
        + "for PAX South dates and badge info.\n"
        + "https://guildofgeeks.net/images/news/pax_2019.gif",
    "RTX": "Welcome to the RTX chat! Please see the pinned messages for RTX "
        + "dates and badge info.\nhttps://tenor.com/FMN3.gif",
    "TRF": "Welcome to the Texas Renaissance Festival chat! We are going as a "
        + "group on the evening of **Friday, October 18th** and staying until "
        + "the afternoon/evening of **Sunday, October 20th**.\n\n**Tickets:** "
        + "https://www.texrenfest.com/tickets-season-passes\n**Address:** "
        + "21778 FM 1774, Todd Mission, TX 77363\n\nSee pinned messages for "
        + "more resources and information.",
    "Austin": "Welcome to the Austin meetups chat!\nhttps://gph.is/2RXIsoC",
    "DFW": "Welcome to the DFW meetups chat!",
    "Waco": "Welcome to the Waco meetups chat!\nhttps://gph.is/2DiUgy8",
    "MHA": "Welcome to My Hero Academia chat!\n Please read the chat "
        + "description for what epsidoes are free to discuss here.\n"
        + "https://gph.is/2KlDpdq",
    "RWBY": "Welcome to RWBY chat!\nPlease read the chat description for what "
        + "volumes are free to discuss here.\nIf a new season is actively "
        + "airing, you may type !roles RWBY Spoilers to get access to the RWBY "
        + "Spoilers chat room.\nhttps://gph.is/2pcsSIw",
    "Utena": "Welcome to Utena chat!\nhttp://gph.is/2u1cdL6",
    "gen:LOCK": "Welcome gen:LOCK chat!\nEverything here is open to discuss "
        + "once it reaches the Rooster Teeth website. If you aren't ready to "
        + "see the spoilers, feel free to mute this channel and open it when "
        + "you are ready.\nRules:\n- Absolutely no tagging people in this chat "
        + "room. At all.\n- Information and content leaked prior to being "
        + "released on the RT site is not open for discussion here. You must "
        + "wait until RT site release.\n"
        + "https://media0.giphy.com/media/3ksxbQNTh5UyWlqf0f/source.gif",
    "Manga Spoilers": "Welcome to the Manga Spoilers chat!\nAny chapter for "
        + "any manga is open to be discussed here one week after its release"
        + "in Japan.\nhttps://gph.is/2yXgWzt",
    "RWBY Spoilers": "Welcome to RWBY Spoilers chat.\nEverything here is open "
        + "to discuss once it reaches the Rooster Teeth website. If you aren't "
        + "ready to see the spoilers, feel free to mute this channel and open "
        + "it when you are ready.\nRules:\n- Absolutely no tagging people in "
        + "this chat room. At all.\n- Information and content leaked prior to "
        + "being released on the RT site is not open for discussion here. You "
        + "must wait until RT site release.\nhttps://gph.is/g/4MWMoqR"
}

module.exports = { findDiff, newRole }